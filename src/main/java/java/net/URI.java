package java.net;

import org.gwtproject.regexp.shared.MatchResult;
import org.gwtproject.regexp.shared.RegExp;

import java.io.Serializable;

public final class URI implements Comparable<URI>, Serializable {

  static final long serialVersionUID = -6052424284110960213L;

  /**
   * This expression derived/taken from the BNF for URI (RFC2396).
   */
  private static final String URL_REGEX =
      "^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?";
  // 12 3 4 5 6 7 8 9
  private static final RegExp URL_PATTERN = RegExp.compile(URL_REGEX);

  /**
   * Schema/Protocol (ie. http:, ftp:, file:, etc).
   */
  private static final int PARSE_URL_SCHEME = 2;

  /**
   * Includes hostname/ip and port number.
   */
  private static final int PARSE_URL_AUTHORITY = 4;

  private static final int PARSE_URL_PATH = 5;

  private static final int PARSE_URL_QUERY = 7;

  private static final int PARSE_URL_FRAGMENT = 9;

  private final String scheme;
  private final String host;
  private final String rawPath;
  private String rawQuery;
  private final String rawFragment;

  public URI(final String str) throws URISyntaxException {
    final MatchResult urlMatcher = URL_PATTERN.exec(str);
    if (urlMatcher == null) {
      throw new URISyntaxException(str, "can not be parsed");
    }
    scheme = urlMatcher.getGroup(PARSE_URL_SCHEME);
    host = urlMatcher.getGroup(PARSE_URL_AUTHORITY);
    rawPath = urlMatcher.getGroup(PARSE_URL_PATH);
    rawQuery = urlMatcher.getGroup(PARSE_URL_QUERY);
    rawFragment = urlMatcher.getGroup(PARSE_URL_FRAGMENT);
  }

  public URI(final String scheme, final String host, final String path, final String fragment)
      throws URISyntaxException {
    this.scheme = scheme;
    this.host = host;
    rawPath = path;
    rawFragment = fragment;
  }

  public URI normalize() {
    return this;
  }

  public String getPath() {
    return rawPath;
  }

  public String getScheme() {
    return scheme;
  }

  public String getSchemeSpecificPart() {
    return scheme;
  }

  public String getRawSchemeSpecificPart() {
    return scheme;
  }

  public String getHost() {
    return host;
  }

  public String getAuthority() {
    return host;
  }

  public String getRawAuthority() {
    return host;
  }

  public String getUserInfo() {
    return null;
  }

  public String getRawUserInfo() {
    return null;
  }

  public String getPort() {
    return null;
  }

  public String getRawPath() {
    return rawPath;
  }

  public String getRawQuery() {
    return rawQuery;
  }

  public String getQuery() {
    return rawQuery;
  }

  public String getRawFragment() {
    return rawFragment;
  }

  public String getFragment() {
    return rawFragment;
  }

  @Override
  public int compareTo(final URI that) {
    if (that == null || that.getPath() == null) {
      return -1;
    }
    if (getPath() == null) {
      return 1;
    }
    return getPath().compareTo(that.getPath());
  }
}

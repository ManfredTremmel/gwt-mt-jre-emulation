package java.util.concurrent;

import java.util.Random;

public class ThreadLocalRandom extends Random {

  private static final long serialVersionUID = 7318848755102526184L;

  /**
   * Returns the current thread's {@code ThreadLocalRandom}.
   *
   * @return the current thread's {@code ThreadLocalRandom}
   */
  public static ThreadLocalRandom current() {
    return new ThreadLocalRandom();
  }

  /** Constructor used only for static singleton */
  private ThreadLocalRandom() {
    super();
  }

  public int nextInt(int minSize, int maxSize) {
    return 0;
  }
}

package java.security;

import java.util.Random;

public class SecureRandom extends Random {

  private static final long serialVersionUID = -7316578004812544591L;

  /**
   * Constructs a secure random number generator (RNG) implementing the default random number
   * algorithm.
   *
   * <p>
   * This constructor traverses the list of registered security Providers, starting with the most
   * preferred Provider. A new SecureRandom object encapsulating the SecureRandomSpi implementation
   * from the first Provider that supports a SecureRandom (RNG) algorithm is returned. If none of
   * the Providers support a RNG algorithm, then an implementation-specific default is returned.
   *
   * <p>
   * Note that the list of registered providers may be retrieved via the
   * {@link Security#getProviders() Security.getProviders()} method.
   *
   * <p>
   * See the SecureRandom section in the
   * <a href= "{@docRoot}/../technotes/guides/security/StandardNames.html#SecureRandom"> Java
   * Cryptography Architecture Standard Algorithm Name Documentation</a> for information about
   * standard RNG algorithm names.
   *
   * <p>
   * The returned SecureRandom object has not been seeded. To seed the returned object, call the
   * {@code setSeed} method. If {@code setSeed} is not called, the first call to {@code nextBytes}
   * will force the SecureRandom object to seed itself. This self-seeding will not occur if
   * {@code setSeed} was previously called.
   */
  public SecureRandom() {
    /*
     * This call to our superclass constructor will result in a call to our own {@code setSeed}
     * method, which will return immediately when it is passed zero.
     */
    super(0);
  }

  /**
   * Constructs a secure random number generator (RNG) implementing the default random number
   * algorithm. The SecureRandom instance is seeded with the specified seed bytes.
   *
   * <p>
   * This constructor traverses the list of registered security Providers, starting with the most
   * preferred Provider. A new SecureRandom object encapsulating the SecureRandomSpi implementation
   * from the first Provider that supports a SecureRandom (RNG) algorithm is returned. If none of
   * the Providers support a RNG algorithm, then an implementation-specific default is returned.
   *
   * <p>
   * Note that the list of registered providers may be retrieved via the
   * {@link Security#getProviders() Security.getProviders()} method.
   *
   * <p>
   * See the SecureRandom section in the
   * <a href= "{@docRoot}/../technotes/guides/security/StandardNames.html#SecureRandom"> Java
   * Cryptography Architecture Standard Algorithm Name Documentation</a> for information about
   * standard RNG algorithm names.
   *
   * @param seed the seed.
   */
  public SecureRandom(final byte seed[]) {
    super(0);
  }

  public static SecureRandom getInstanceStrong()
          throws NoSuchAlgorithmException {
    return new SecureRandom();
  }
}

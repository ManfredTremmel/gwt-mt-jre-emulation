# gwt-mt-jre-emulation

Additional jre emulation classes for gwt and j2cl.


Maven integration
----------------

Add the dependencies itself for GWT-Projects (use source package, there's no need for the binary jar):

```xml
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-mt-jre-emulation</artifactId>
      <version>1.0.6</version>
      <classifier>sources</classifier>
    </dependency>
```

GWT Integration
---------------

What you still have to do, inherit Decorators into your project .gwt.xml file:

```
<inherits name="de.knightsoftnet.mtjre.GwtMtJreEmulation" />
```
